import os
import random

import numpy as np
import tensorflow as tf
from keras.layers import Dense, GlobalAveragePooling2D, Input, Flatten, Dropout
from keras.models import Model
from tensorflow.keras.applications import EfficientNetB0
import matplotlib.pyplot as plt

from utils.generator import DataGenerator

random.seed(42)
np.random.seed(42)
tf.random.set_seed(42)

data_folder = "D:\Baza danych\Threshold_test\chunkedfull/"

def main():

    partition = {"train": [], "validation": [], "test": []}

    for file in os.listdir(data_folder + "train"):
        if file.startswith("id-tr"):
            partition["train"].append(str(file.split(".")[0]))
    for file in os.listdir(data_folder + "valid"):
        if file.startswith("id-val"):
            partition["validation"].append(str(file.split(".")[0]))
    for file in os.listdir(data_folder + "test"):
        if file.startswith("id-te"):
            partition["test"].append(str(file.split(".")[0]))

    train_generator = DataGenerator(partition["train"], sub="train")
    valid_generator = DataGenerator(partition["validation"], sub="valid")
    test_generator = DataGenerator(partition["test"], sub="test")

    base_model = EfficientNetB0(
        weights="imagenet", include_top=False, input_shape=(134, 390, 3))
    base_model.trainable = False
    layers = base_model.layers
    layers[-1].trainable = True
    layers[-2].trainable = True
    layers[-3].trainable = True
    layers[-4].trainable = True
    layers[-5].trainable = True

    inputs = Input(shape=(134, 390, 3))
    x = base_model(inputs, training=False)
    x = GlobalAveragePooling2D()(x)
    x = Dense(500, activation="relu")(x)
    x = Dropout(0.5)(x)
    outputs = Dense(2, activation="softmax")(x)
    model = Model(inputs, outputs)

    model.compile(loss="categorical_crossentropy", optimizer="adam", metrics=["accuracy"])

    model.summary()

    history = model.fit(x=train_generator, validation_steps=None, validation_data=valid_generator, epochs=50,
                        batch_size=100)

    printhist(history)

    model.evaluate(test_generator, verbose=1)


def printhist(history):
    plt.plot(history.history['accuracy'])
    plt.plot(history.history['val_accuracy'])
    plt.title('model accuracy')
    plt.ylabel('accuracy')
    plt.xlabel('epoch')
    plt.legend(['train', 'validation'], loc='upper left')
    plt.show()

    plt.plot(history.history['loss'])
    plt.plot(history.history['val_loss'])
    plt.title('model loss')
    plt.ylabel('loss')
    plt.xlabel('epoch')
    plt.legend(['train', 'validation'], loc='upper left')
    plt.show()


if __name__ == "__main__":
    main()
