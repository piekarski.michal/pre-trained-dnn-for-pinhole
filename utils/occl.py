from copy import deepcopy

import h5py
import keras
import matplotlib.pyplot as plt
import numpy as np
import tensorflow as tf

IMG_SIZE = [134,390]

preprocess_input = tf.keras.applications.inception_v3.preprocess_input


def reformat(input):
    x = np.zeros((len(input), IMG_SIZE[0], IMG_SIZE[1], 3))
    for i in range(len(input)):
        x[i, :, :, 0] = input[i][:, :, 0]
        x[i, :, :, 1] = input[i][:, :, 1]
        x[i, :, :, 2] = input[i][:, :, 2]

    # Note: currently using RGB format

    return x


class OcclusionGenerator(object):
    def __init__(self, img, boxsize=10, step=10):
        """ Initializations """
        self.img = img
        self.boxsize = boxsize
        self.step = step
        self.i = 0
        self.j = 0

    def flow(self):
        """ Return a single occluded image and its location """
        if self.i + self.boxsize > self.img.shape[0]:
            return None, None, None

        retImg = np.copy(self.img)
        retImg[self.i : self.i + self.boxsize, self.j : self.j + self.boxsize] = 0.0

        old_i = deepcopy(self.i)
        old_j = deepcopy(self.j)

        # update indices
        self.j = self.j + self.step
        if self.j + self.boxsize > self.img.shape[1]:  # reached end
            self.j = 0  # reset j
            self.i = self.i + self.step  # go to next row

        return retImg, old_i, old_j

    def gen_minibatch(self, batchsize=10):
        """ Returns a minibatch of images of size <=batchsize """

        # list of occluded images
        occ_imlist = []
        locations = []
        for i in range(batchsize):
            occimg, i, j = self.flow()
            if occimg is not None:
                occ_imlist.append(occimg)
                locations.append([i, j])

        if len(occ_imlist) == 0:  # no data
            return None, None
        else:
            # convert list to numpy array and pre-process input (0 mean centering)
            x = preprocess_input(reformat(occ_imlist))
            return x, locations


def post_process(heatmap):
    # postprocessing
    total = heatmap[0]
    for val in heatmap[1:]:
        total = total + val

    return total


def gen_heatmap(img, model, boxsize, step):

    x = np.expand_dims(img, axis=0)
    x = preprocess_input(x)

    preds = model.predict(x)
    correct_class_index = np.argmax(preds[0])
    # generate occluded images and location of mask
    occ = OcclusionGenerator(img, boxsize, step)

    # scores of occluded image
    heatmap = []
    index = 0
    while True:

        # get minibatch of data
        x, locations = occ.gen_minibatch(batchsize=10)
        if x is not None:

            # predict
            op = model.predict(x)

            # unpack prediction values
            for i in range(x.shape[0]):
                score = op[i][correct_class_index]
                r, c = locations[i]
                scoremap = np.zeros((IMG_SIZE[0], IMG_SIZE[1]))
                scoremap[r : r + occ.boxsize, c : c + occ.boxsize] = score
                heatmap.append(scoremap)
        else:
            break

    return heatmap, correct_class_index


def main():
    model = keras.models.load_model("../results/IncV3/model_incV3")
    IMAGE = 10559
    FILE = "dataset_part1"
    data = f"D:\Baza danych\Threshold_test\{FILE}.h5"

    hf = h5py.File(data, "r")
    x = hf.get("image_data")
    y = hf.get("labels")
    x = x[()]
    y = y[()]
    hf.close()

    img = x[10559]
    img = np.stack((img, img, img), axis=-1)

    heatmapList, index = gen_heatmap(img, model, 20, 10)
    processed = post_process(heatmapList)

    plt.subplot(121)
    plt.imshow(img)
    plt.axis("off")
    plt.title("IMG")
    plt.subplot(122)
    plt.imshow(processed)
    plt.axis("off")
    plt.show()


if __name__ == "__main__":
    main()
