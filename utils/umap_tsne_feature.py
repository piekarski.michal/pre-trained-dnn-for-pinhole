import os

import h5py
import keras
import matplotlib.pyplot as plt
import numpy as np
from keras.layers import Flatten
from keras.models import Model
from sklearn.manifold import TSNE
from umap import UMAP

from utils.generator import DataGenerator

data_folder = "D:\Baza danych\Threshold_test\chunkedfinal/"
reconstructed_model = keras.models.load_model("../results/IncV3/model_incV3")
IMAGES = 30000
FILE = "dataset_part1"
data = f"D:\Baza danych\Threshold_test\{FILE}.h5"


hf = h5py.File(data, "r")
x = hf.get("image_data")
y = hf.get("labels")
x = x[()]
y = y[()]
hf.close()

inp = reconstructed_model.input
t = reconstructed_model.layers[1](inp)
out = Flatten()(t)
model2 = Model(inp, out)
model2.summary()

partition = {"visual1": [], "visual2": [], "visual3": []}
for file in os.listdir(data_folder + "visual1"):
    if file.startswith("id-tr"):
        partition["visual1"].append(str(file.split(".")[0]))
for file in os.listdir(data_folder + "visual2"):
    if file.startswith("id-tr"):
        partition["visual2"].append(str(file.split(".")[0]))
for file in os.listdir(data_folder + "visual3"):
    if file.startswith("id-tr"):
        partition["visual3"].append(str(file.split(".")[0]))

visual_generator1 = DataGenerator(partition["visual1"], sub="visual1")
visual_generator2 = DataGenerator(partition["visual2"], sub="visual2")
visual_generator3 = DataGenerator(partition["visual3"], sub="visual3")

y_train = y[:IMAGES]
pred1 = model2.predict(visual_generator1)
pred2 = model2.predict(visual_generator2)
pred3 = model2.predict(visual_generator3)

pred = np.concatenate((pred1, pred2, pred3))

# Project data with UMAP
reducer = UMAP(n_components=2, n_neighbors=15, random_state=0)
reducer.fit(pred)
galaxy10_umap = reducer.transform(pred)
fig1 = plt.figure()
plt.scatter(
    galaxy10_umap[:, 0],
    galaxy10_umap[:, 1],
    c=y_train,
    label=y_train,
)
plt.title("UMAP projection of InceptionV3-based feature extraction")
plt.show()

# Project data with tSNE
tsne = TSNE(n_components=2, random_state=0).fit_transform(pred)
fig2 = plt.figure()
plt.scatter(
    tsne[:, 0],
    tsne[:, 1],
    c=y_train,
    label=y_train,
)
plt.title("tSNE projection InceptionV3-based feature extraction")
plt.show()
