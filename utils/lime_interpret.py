import os
import sys

import h5py
import keras
import matplotlib.pyplot as plt
import numpy as np
import tensorflow as tf
from lime import lime_image
from skimage.segmentation import mark_boundaries

IMG_SIZE = [134, 390]

preprocess_input = tf.keras.applications.inception_v3.preprocess_input


def main():
    inet_model = keras.models.load_model("../results/IncV3/model_incV3")
    IMAGE = 10559
    FILE = "dataset_part1"
    data = f"D:\Baza danych\Threshold_test\{FILE}.h5"

    hf = h5py.File(data, "r")
    x = hf.get("image_data")
    y = hf.get("labels")
    x = x[()]
    y = y[()]
    hf.close()

    img = x[10559]
    img = np.stack((img, img, img), axis=-1)

    x = np.expand_dims(img, axis=0)
    x = preprocess_input(x)

    preds = inet_model.predict(x)

    explainer = lime_image.LimeImageExplainer()

    explanation = explainer.explain_instance(
        x[0].astype("double"),
        inet_model.predict,
        top_labels=2,
        hide_color=0,
        num_samples=10000,
    )

    temp, mask = explanation.get_image_and_mask(
        explanation.top_labels[0], positive_only=False, num_features=10, hide_rest=False, #min_weight=1e-11
    )
    plt.imshow(mark_boundaries(temp / 2 + 0.5, mask))
    plt.axis("off")
    plt.show()

    temp, mask = explanation.get_image_and_mask(
        explanation.top_labels[0], positive_only=False, num_features=12, hide_rest=False,  #min_weight=1e-11
    )
    plt.imshow(mark_boundaries(temp / 2 + 0.5, mask))
    plt.axis("off")
    plt.show()

    temp, mask = explanation.get_image_and_mask(
        explanation.top_labels[0], positive_only=False, num_features=15, hide_rest=False, #min_weight=1e-11
    )
    plt.imshow(mark_boundaries(temp / 2 + 0.5, mask))
    plt.axis("off")
    plt.show()

    temp, mask = explanation.get_image_and_mask(
        explanation.top_labels[0], positive_only=False, num_features=20, hide_rest=False,  #min_weight=1e-11
    )
    plt.imshow(mark_boundaries(temp / 2 + 0.5, mask))
    plt.axis("off")
    plt.show()

    temp, mask = explanation.get_image_and_mask(
        explanation.top_labels[0], positive_only=False, num_features=7, hide_rest=False,  # min_weight=1e-11
    )
    plt.imshow(mark_boundaries(temp / 2 + 0.5, mask))
    plt.axis("off")
    plt.show()

    temp, mask = explanation.get_image_and_mask(
        explanation.top_labels[0], positive_only=False, num_features=8, hide_rest=False,  # min_weight=1e-11
    )
    plt.imshow(mark_boundaries(temp / 2 + 0.5, mask))
    plt.axis("off")
    plt.show()

    temp, mask = explanation.get_image_and_mask(
        explanation.top_labels[0], positive_only=False, num_features=9, hide_rest=False,  # min_weight=1e-11
    )
    plt.imshow(mark_boundaries(temp / 2 + 0.5, mask))
    plt.axis("off")
    plt.show()

    # # Select the same class explained on the figures above.
    # ind = explanation.top_labels[0]
    #
    # # Map each explanation weight to the corresponding superpixel
    # dict_heatmap = dict(explanation.local_exp[ind])
    # heatmap = np.vectorize(dict_heatmap.get)(explanation.segments)
    #
    # # Plot. The visualization makes more sense if a symmetrical colorbar is used.
    # plt.imshow(heatmap, cmap="RdBu", vmin=-heatmap.max(), vmax=heatmap.max())
    # plt.colorbar()
    # plt.axis("off")
    # plt.show()


if __name__ == "__main__":
    main()
