import os

import h5py
from IPython.display import Image, display
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import numpy as np
from keras.models import Model, Input
import tensorflow as tf
from tensorflow import keras


def get_img_array(img, size):
    # `img` is a PIL image of size 299x299
    img = np.stack((img, img, img), axis=-1)
    # `array` is a float32 Numpy array of shape (299, 299, 3)
    array = keras.preprocessing.image.img_to_array(img)
    # We add a dimension to transform our array into a "batch"
    # of size (1, 299, 299, 3)
    array = np.expand_dims(array, axis=0)
    return array


def make_gradcam_heatmap(img_array, model, last_conv_layer_name, pred_index=None):
    # First, we create a model that maps the input image to the activations
    # of the last conv layer as well as the output predictions

    last_conv_layer = model.layers[1].layers[-1]
    last_conv_layer_model = keras.Model(model.layers[1].inputs, last_conv_layer.output)

    classifier_input = keras.Input(shape=last_conv_layer.output.shape[1:])
    x = classifier_input
    for layer in model.layers[-4:]:
        x = layer(x)
    classifier_model = keras.Model(classifier_input, x)

    preprocess_layers = keras.Model(model.inputs, model.layers[-6].output)
    img_array = preprocess_layers(img_array)

    # grad_model = tf.keras.models.Model(
    #     [model.input], [model.get_layer('inception_v3').get_layer('conv2d_93'), model.output]
    # )


    # Then, we compute the gradient of the top predicted class for our input image
    # with respect to the activations of the last conv layer
    with tf.GradientTape() as tape:
        last_conv_layer_output = last_conv_layer_model(img_array)
        tape.watch(last_conv_layer_output)
        # Compute class predictions
        preds = classifier_model(last_conv_layer_output)
        top_pred_index = tf.argmax(preds[0])
        top_class_channel = preds[:, top_pred_index]

    # This is the gradient of the output neuron (top predicted or chosen)
    # with regard to the output feature map of the last conv layer
    # grads = tape.gradient(class_channel, last_conv_layer_output)

    grads = tape.gradient(top_class_channel, last_conv_layer_output)

    # This is a vector where each entry is the mean intensity of the gradient
    # over a specific feature map channel
    pooled_grads = tf.reduce_mean(grads, axis=(0, 1, 2))

    # We multiply each channel in the feature map array
    # by "how important this channel is" with regard to the top predicted class
    # then sum all the channels to obtain the heatmap class activation
    last_conv_layer_output = last_conv_layer_output[0]
    heatmap = last_conv_layer_output @ pooled_grads[..., tf.newaxis]
    heatmap = tf.squeeze(heatmap)

    # For visualization purpose, we will also normalize the heatmap between 0 & 1
    heatmap = tf.maximum(heatmap, 0) / tf.math.reduce_max(heatmap)
    return heatmap.numpy()

def main():
    data_folder = "/home/michal/umaps/"
    reconstructed_model = keras.models.load_model("../results/IncV3/model_incV3")
    FILE = "dataset_part1"
    data = f"D:\Baza danych\Threshold_test\{FILE}.h5"

    hf = h5py.File(data, "r")
    x = hf.get("image_data")
    y = hf.get("labels")
    x = x[()]
    y = y[()]
    hf.close()

    img_size = (134, 390)
    preprocess_input = tf.keras.applications.inception_v3.preprocess_input
    #decode_predictions = tf.keras.applications.inception_v3.decode_predictions

    img = x[34000]

    # Prepare image
    img_array = preprocess_input(get_img_array(img, size=img_size))

    preds = reconstructed_model.predict(img_array)

    # Generate class activation heatmap
    heatmap = make_gradcam_heatmap(img_array, reconstructed_model, 'activation_93')

    # Display heatmap
    plt.matshow(heatmap)
    #plt.title('GradCAM')
    plt.axis("off")
    plt.show()


if __name__ == "__main__":
    main()
