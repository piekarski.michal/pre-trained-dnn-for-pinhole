import h5py
import matplotlib.pyplot as plt
import numpy as np
from sklearn.manifold import TSNE
from umap import UMAP, plot

IMAGES = 30000
FILE = "dataset_part1"
data = f"D:\Baza danych\Threshold_test\{FILE}.h5"

hf = h5py.File(data, "r")
x = hf.get("image_data")
y = hf.get("labels")
x = x[()]
y = y[()]
hf.close()

flatten_size = x.shape[1] * x.shape[2]

X_train = np.empty([IMAGES, flatten_size], dtype=np.uint8)
y_train = np.empty([IMAGES], dtype=np.bool)

# Get a subset of the data
for i in range(IMAGES):
    X_train[i, :] = np.array(np.ndarray.flatten(x[i, :, :]), dtype=np.uint8)
    y_train[i] = y[i]

# Plot distribution
classes, frequency = np.unique(y_train, return_counts=True)
fig = plt.figure(1, figsize=(4, 4))
plt.clf()
plt.bar(classes, frequency)
plt.xlabel("Class")
plt.ylabel("Frequency")
plt.title("Data Subset")
plt.show()

# Project data with UMAP
reducer = UMAP(n_components=2, n_neighbors=15, random_state=0)
reducer.fit(X_train)
galaxy10_umap = reducer.transform(X_train)
fig1 = plt.figure()
plt.scatter(
    galaxy10_umap[:, 0],
    galaxy10_umap[:, 1],
    c=y_train,
    label=y_train,
)
plt.title("UMAP projection of Pinhole dataset")
plt.show()

# Project data with tSNE
tsne = TSNE(n_components=2).fit_transform(X_train)
fig2 = plt.figure()
plt.scatter(
    tsne[:, 0],
    tsne[:, 1],
    c=y_train,
    label=y_train,
)
plt.title("tSNE projection of Pinhole dataset")
plt.show()
