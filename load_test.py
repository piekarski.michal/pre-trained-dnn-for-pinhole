import keras
import numpy as np
import tensorflow as tf

# data location
data = ""

with open(data, "rb") as f:
    _x = np.load(f)

item = tf.keras.applications.inception_v3.preprocess_input(
    np.expand_dims(_x[0], axis=0)
)

model = keras.models.load_model("utils/best_incV3.h5")

model.summary()

predictions = model.predict(item)

print(predictions)
