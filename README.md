# Pre-trained DNN for Pinhole


## Getting started 

This repository contains a pre-trained architectures for raw pinhole images classification as well as some util scripts
(data chunking, UMAP representation).

## Authors and acknowledgment
Michal Piekarski 

## License
GNU General Public License v3.0
